document.addEventListener("DOMContentLoaded", function() {

    const button = document.getElementById("darkmode"); 
    button.addEventListener("click", function(){
        const body = document.body;
        
        if (body.classList.contains("dark-mode")){
            body.classList.remove("dark-mode");
        }
        else{
            body.classList.add("dark-mode");
        }
        
    });

});
