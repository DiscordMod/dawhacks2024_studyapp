class Day:
    def __init__(self, month, day, evaluation="", exams_to_study=[]):
        self.month = month
        self.day = day
        self.evaluation = evaluation
        self.exams_to_study = exams_to_study
    
    def __str__(self):
        result = f"{self.day} "
        for exam in self.exams_to_study:
            result += f"\n study for: \n{exam}"
        return result
