from PIL import Image
import pytesseract
import re
from datetime import datetime
# Configure Tesseract

def add_date(date,text,entries):
    entry = {'date': date,'text': text}
    entries[date] = entry
def transform_day(day_str):
# Regular expression to match the day, month, and year components
    pattern = r'(\d{1,2}) (\w{3})'
    match = re.match(pattern, day_str)
    if match:
    # Extract the day and month
        day, month = match.groups()
    
    # Convert the month name to its corresponding month number
        month_number = datetime.strptime(month, '%b').month
    
    # Format the day, month, and year into the desired format
    # Assuming the year is 2024 for this example
        formatted_day = f"{datetime.now().year}/{month_number:02d}/{day}"
    
        return formatted_day
    else:
        return "Invalid format"
def main(img):
    entries = {}
    tessdata_dir_config = r'--tessdata-dir "bin\tessdata"'
    pytesseract.pytesseract.tesseract_cmd = r'bin\Tesseract\tesseract.exe'

# Open an image file
# Use pytesseract to convert the image to text
    text = pytesseract.image_to_string(Image.open(img), config=tessdata_dir_config)

# Split the into an array at every newline
    formatted_text_list = text.splitlines()
# makes an array of string where line isnt empty and starts with a number
    new_formatted_text_list = []
    for line in formatted_text_list:
        if line == "":
            continue
        if line[0].isdigit() and (line[1].isdigit() or line[1] == " "):
            new_formatted_text_list.append(line)

#the regex pattern to match the day and month components
    pattern = r"(\d{1,2})([A-Za-z]{3})"
    replacement = r"\1 \2"

# Use the re.sub() function to replace the pattern with the replacement
    new_formatted_text_list =[re.sub(pattern, replacement, line) for line in new_formatted_text_list]
# Regex pattern to match a day (one or two digits) followed by a month (three letters)
# and insert a space between them
    pattern = r"\d{1,2}\s[A-Za-z]{3}"

    for line in new_formatted_text_list:
    # Split the line at the pattern
        split_text_after = re.split(pattern, line)
    #validation for the string
        if len(split_text_after) == 1 or split_text_after[1] == "":
            continue
    #escaping the pattern
        escaped_pattern = re.escape(split_text_after[1])
    #split after
        split_text_before = re.split(rf"{escaped_pattern}", line,flags=re.DOTALL)
    #append to array
        add_date(transform_day(split_text_before[0]),split_text_after[1],entries)
    return entries

# def return_entries(img):
#     entries = {}
#     main(img,entries)
#     for date, entry in entries.items():
#         print(f"{date}: {entry['text']}")
#     return entries