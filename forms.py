from flask_wtf import FlaskForm
from wtforms import SubmitField, FileField
from flask_wtf.file import FileRequired, FileAllowed

class ImageForm(FlaskForm):
    image = FileField('image', validators=[FileRequired(), FileAllowed(['jpg', 'png', 'jpeg', 'pdf'], 'Images only!')])
    submit = SubmitField('submit')