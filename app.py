import os
from flask import Flask, redirect, render_template, url_for
from forms import ImageForm
from werkzeug.utils import secure_filename
from month import Month
from day import Day
import calendar
from datetime import datetime,timedelta, date
from tess import *
from markupsafe import escape
app = Flask(__name__)
app.config['SECRET_KEY'] = 'b0cf6ccb21b188cac88713db354127e6'

schoolwork = {}
@app.route("/", methods=['GET', 'POST'])
@app.route("/home", methods=['GET', 'POST'])
def home():
    form = ImageForm()

    if form.validate_on_submit():
        f = form.image.data
        filename = secure_filename(f.filename)
        # MAKE SURE DIRECTORY EXISTS
        os.makedirs(os.path.join(app.instance_path, 'images'), exist_ok=True)
        #SAVE FILE TO INSTANCE FOLDER
        f.save(os.path.join( app.instance_path, 'images', filename))
        #call function with filename from FileStorage object
        return redirect (url_for("schedule",month_to_display="January"))
    return render_template('home.html', form=form)

@app.route("/schedule/<string:month_to_display>")
def schedule(month_to_display):
    #pass in a month object?, or just a list with days associated with the month?
    month_to_display = escape(month_to_display)
    month_object = None
    schoolwork = main(f'{app.instance_path}\\images\\{'test4.png'}')
    months=youran(schoolwork)
    for month in months:
        if month.name == month_to_display:
            month_object = month
            break
    month_index = months.index(month_object)
    if month_index < 0:
        prev_month=months[month_index]
    if month_index > 11:
        next_month=months[month_index]
    prev_month = months[month_index - 1]
    next_month = months[month_index + 1]
    weeks = []
    week = []
    for i in range(len(month_object.days)):
        week.append(month_object.days[i])
        if (i % 7 == 0):
            weeks.append(week)
            week = []
    weeks.append(week)
    return render_template('schedule.html', weeks=weeks, month=month_object, prev_month=prev_month, next_month=next_month) 


def youran(schoolwork):
    months_strings_LMFAOOO = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
    ]
    months = []
    for i in range(1,13):
        days_of_month = calendar.monthrange(datetime.now().year, i)[1]
        days = []
        examsday = []
        examstudydays = []
        examday = False
        examcoming = False
        for date in schoolwork.items():
            examsday.append(date)
        for exam in examsday:
            date_object = datetime.strptime(exam[0], "%Y/%m/%d")
            for k in range(1, 8):
                result = (date_object - timedelta(days=k)).date()
                examstudydays.append(f"{result.year}/{result.month}/{result.day}")
                
        for j in range(1, days_of_month+1):
            if f"{datetime.now().year}/{i}/{j}" in examsday:
                examday = True
            if f"{datetime.now().year}/{i}/{j}" in examstudydays:
                examcoming = True
            if examday:
                days.append(Day(datetime.now().month, j, evaluation=schoolwork[f"{datetime.now().year}/{i}/{j}"].text))
            elif examcoming:
                examstostudy = []
                date_object = datetime.strptime(f"{datetime.now().year}/{i}/{j}", "%Y/%m/%d")
                for exam in examsday:
                    if datetime.strptime(exam[0], "%Y/%m/%d") - date_object <= timedelta(days=7):
                        examstostudy.append(schoolwork[exam[0]]['text'])
                days.append(Day(datetime.now().month, j, exams_to_study=examstostudy))
            elif examcoming and examday:
                examstostudy = []
                date_object = datetime.strptime(f"{datetime.now().year}/{i}/{j}", "%Y/%m/%d")
                for exam in examsday:
                    if datetime.strptime(exam[0], "%Y/%m/%d") - date_object <= timedelta(days=7):
                        examstostudy.append([schoolwork[exam].text])
                days.append(Day(datetime.now().month, j, evaluation=schoolwork[f"{datetime.now().year}/{i}/{j}"].text), examstostudy)
            else:
                days.append(Day(datetime.now().month, j))
        
        months.append(Month(days, months_strings_LMFAOOO[i-1], 2044))
    return months

if __name__ == "__main__":
    app.run(port=5320, debug=True)
